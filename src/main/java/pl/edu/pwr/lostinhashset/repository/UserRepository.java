package pl.edu.pwr.lostinhashset.repository;

import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;

public interface UserRepository {

	public List<User> getUsers();
	
}
