package pl.edu.pwr.lostinhashset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import pl.edu.pwr.lostinhashset.entity.Address;
import pl.edu.pwr.lostinhashset.entity.User;

public class ToString {

	public static void main(String[] args) {

		System.out.println("---------------------------------------------------");
		
		Object object = new Object();
		System.out.println("Objekt przedstawia się swoim typem i hash-kodem: \n" + object.toString());
		System.out.println("Metoda .toString() wywoływana jest \"po cichu\": \n" + object);
		System.out.println("---------------------------------------------------");
		
		double number = 7.3;
		System.out.println("Typy prymitywne nie mają metody .toString(): " + number);
		
		Integer bigNumber = 7;
		System.out.println("Objekt typu Integer przedstawia się swoją wartością: \n" + bigNumber);
		
		Boolean flag = Boolean.FALSE;
		System.out.println("Objekt typu Boolean przedstawia się swoją wartością: \n" + flag);
		System.out.println("---------------------------------------------------");
		
		Address address = getPWRAddress();
		System.out.println("Objekt typu Address ma nadpisaną metodę .toString() i przedstawia się: \n" + address);
		System.out.println("---------------------------------------------------");
		
		User[] users = new User[3];
		System.out.println("Pusta tablica przedstawia się swoim typem i hash-kodem: \n" + users);
		users[0] = new User();
		users[1] = new User();
		users[2] = new User();
		System.out.println("Tablica zawierająca trzy elementy nadal przedstawia się swoim typem i hash-kodem: \n" + users);
		Integer[] numbers = new Integer[2];
		numbers[0] = 7;
		numbers[1] = 5;
		System.out.println("Niezależnie od tego jakiego typu obiekty zawiera: \n" + numbers);
		System.out.println("---------------------------------------------------");
		
		List<Address> addresses = new ArrayList<Address>();
		System.out.println("Pusta kolekcja przedstawia się w nawiasach kwadratowych: \n" + addresses.toString());
		
		Set<Address> addresses2 = new HashSet<Address>();
		addresses2.add(address);
		System.out.println("Na elementach kolekcji wywoływana jest metoda .toString(): \n" + addresses2);
		
		Queue<User> usersQueue = new LinkedList<User>();
		usersQueue.offer(new User());
		System.out.println(usersQueue);
		System.out.println("---------------------------------------------------");
		
		Map<String, Object> politechnika = new HashMap<String, Object>();
		politechnika.put("liczba studentów", 30000);
		politechnika.put("adres", address);
		System.out.println("Mapa przedstawia się w klamrach w parach klucz=wartość: \n" + politechnika);
		System.out.println("---------------------------------------------------");
				
		System.out.println("Wywołanie null.toString() rzuci wyjątkiem NullPointerException \n");
//		Object nullObject = null;
//		System.out.println(nullObject.toString());
		
		//Poniższa linia się nie skompiluje
//		System.out.println(null.toString());
		
	}

	private static Address getPWRAddress() {
		Address address = new Address();
		address.setStreet("Wybrzeże Wyspiańskiego");
		address.setHomeNo("27");
		address.setZipCode("50-370");
		address.setTown("Wrocław");
		return address;
	}
	
}
